/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use steam_audio_rs::{Context, RenderingSettings, HrtfParams, AudioFormatBuilder};
use steam_audio_rs::enumerators::{ConvolutionType, HrtfDatabaseType, ChannelLayout};

const SAMPLE_RATE: i32 = 44100;
const BUFFER_SIZE: i32 = 4096;
const FRAME_SIZE:  i32 = 1024;

fn init_steam_audio () -> Result<(), String> {

    // Create the rendering settings
    let settings   = RenderingSettings::new (SAMPLE_RATE, FRAME_SIZE, ConvolutionType::Phonon);
    let hrtf_param = HrtfParams::new (HrtfDatabaseType::Default, None);

    let input_format = AudioFormatBuilder::default ()
        .build ()?;

    let output_format = AudioFormatBuilder::default ()
        .channel_layout (ChannelLayout::Stereo)
        .build ()?;

    // Create a new audio context and binaural renderer.
    let context  = Context::new ()?;
    let renderer = context.create_binaural_renderer (settings, hrtf_param)?;
    let effect   = renderer.create_binaural_effect (input_format, output_format)?;

    Ok (())
}

fn init_sdl2 () -> Result<(), String> {

    Ok (())
}

fn main () {

    if let (Err (e), _) | (_, Err (e)) = (init_steam_audio (), init_sdl2 ()) {
        panic! ("Failed to initialize: {}", e)
    }


}

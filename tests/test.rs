/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use steam_audio_rs::{AudioFormatBuilder, Context, RenderingSettings, HrtfParams, AudioBuffer};
use steam_audio_rs::enumerators::{ConvolutionType, HrtfDatabaseType, ChannelLayout, HrtfInterpolation};
use steam_audio_rs::Vector3;

const SAMPLE_RATE: i32 = 44100;
const BUFFER_SIZE: i32 = 4096;
const FRAME_SIZE:  i32 = 1024;

#[test]
fn basic_binaural_renderer () {

    let settings = RenderingSettings::new (SAMPLE_RATE, BUFFER_SIZE, ConvolutionType::Phonon);
    let params   = HrtfParams::new (HrtfDatabaseType::Default, None);

    let format = AudioFormatBuilder::default ()
        .channel_layout (ChannelLayout::Stereo)
        .build ()
        .expect ("Failed to create audio format");

    let context  = Context::new ().expect ("Failed to create context");
    let renderer = context.create_binaural_renderer (settings, params).expect ("Failed to create renderer");
    let effect   = renderer.create_binaural_effect (format.clone (), format.clone ()).expect ("Failed to create effect");

    let audio_buffer = AudioBuffer::new (format.clone (), FRAME_SIZE, vec![6f32; BUFFER_SIZE as usize]);
    let audio_buffer = effect.apply_binaural_effect (audio_buffer, Vector3 { x: 1.2, y: 24.0, z: 0.0}, HrtfInterpolation::Nearest);
}

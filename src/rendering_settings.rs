/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLRenderingSettings;
use crate::enumerators::ConvolutionType;

pub struct RenderingSettings {

    pub sampling_rate: i32,
    pub frame_size: i32,
    pub convolution_type: ConvolutionType
}

impl RenderingSettings {

    pub fn new (sampling_rate: i32, frame_size: i32, convolution_type: ConvolutionType) -> RenderingSettings {
        RenderingSettings { sampling_rate, frame_size, convolution_type }
    }
}

impl RenderingSettings {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLRenderingSettings {

        IPLRenderingSettings {

            samplingRate: self.sampling_rate,
            frameSize: self.frame_size,
            convolutionType: self.convolution_type.as_ipl ()
        }
    }
}

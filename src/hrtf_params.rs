/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLHrtfParams;
use crate::enumerators::HrtfDatabaseType;
use std::ptr;

/// Parameters used to describe the HRTF database you want to use when creating a Binaural Renderer object.
pub struct HrtfParams {

    /// Type of HRTF database to use.
    pub database_type: HrtfDatabaseType,

    /// Name of the SOFA file from which to load HRTF data.
    ///
    /// Can be a relative or absolute path. Must be a null-terminated UTF-8 string.
    pub sofa_file_name: String
}

impl HrtfParams {

    pub fn new (database_type: HrtfDatabaseType, sofa_file_name: Option<String>) -> HrtfParams {
        HrtfParams { database_type, sofa_file_name: sofa_file_name.unwrap_or_default () }
    }

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLHrtfParams {

        IPLHrtfParams {

            type_:        self.database_type.as_ipl (),
            sofaFileName: self.sofa_file_name.as_ptr () as *mut _,
            hrtfData:     ptr::null_mut ()
        }
    }
}

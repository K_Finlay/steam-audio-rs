/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

#![allow (non_upper_case_globals)]
#![allow (non_camel_case_types)]
#![allow (non_snake_case)]
#![allow (dead_code)]
include! (concat! (env! ("OUT_DIR"), "/binding.rs"));

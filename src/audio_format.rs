/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::enumerators::{ChannelLayoutType, ChannelLayout, AmbisonicsOrdering, AmbisonicsNormalization, ChannelOrder};
use crate::Vector3;

use derive_builder::Builder;

use crate::binding::IPLAudioFormat;

/// The format of an audio buffer.
///
/// Whenever you pass audio data to or from Phonon, you must describe the format in which the audio is encoded.<br>
/// **Phonon only supports uncompressed PCM wave data, stored in 32-bit floating point format.**<br>
/// However, Phonon supports many different multi-channel and Ambisonics formats, and the IPLAudioFormat tells Phonon how to interpret a buffer of audio data.
#[derive  (Builder, Clone, Debug)]
#[builder (setter (into), default)]
pub struct AudioFormat {

    /// Indicates whether or not the audio should be interpreted as Ambisonics data.
    pub channel_layout_type: ChannelLayoutType,

    /// Specifies the speaker configuration used for multi-channel, speaker-based audio data.
    ///
    /// Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_AMBISONICS.
    pub channel_layout: ChannelLayout,

    /// The number of channels in the audio data.
    ///
    /// Only used if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS and channelLayout is IPL_CHANNELLAYOUT_CUSTOM.
    pub num_speakers: i32,

    /// An array of IPLVector3 objects indicating the direction of each speaker relative to the user.
    ///
    /// Can be NULL. Only used if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS and channelLayout is IPL_CHANNELLAYOUT_CUSTOM.
    pub speaker_directions: Vec<Vector3>,

    /// The order of Ambisonics to use.
    ///
    /// Must be between 0 and 3. Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS.
    pub ambisonics_order: i32,

    /// The ordering of Ambisonics channels within the data.
    ///
    /// Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS.
    pub ambisonics_ordering: AmbisonicsOrdering,

    /// The normalization scheme used for Ambisonics data.
    ///
    /// Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS.
    pub ambisonics_normalization: AmbisonicsNormalization,

    /// Whether the audio data is interleaved or deinterleaved.
    pub channel_order: ChannelOrder
}

impl AudioFormat {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLAudioFormat {

        IPLAudioFormat {

            channelLayoutType:       self.channel_layout_type.as_ipl (),
            channelLayout:           self.channel_layout.as_ipl (),
            numSpeakers:             self.num_speakers,
            speakerDirections:       self.speaker_directions.as_ptr () as *mut _,
            ambisonicsOrder:         self.ambisonics_order,
            ambisonicsOrdering:      self.ambisonics_ordering.as_ipl (),
            ambisonicsNormalization: self.ambisonics_normalization.as_ipl (),
            channelOrder:            self.channel_order.as_ipl (),
        }
    }
}

impl Default for AudioFormat {

    fn default () -> AudioFormat {

        AudioFormat {

            channel_layout_type: ChannelLayoutType::Speakers,
            channel_layout: ChannelLayout::Mono,
            num_speakers: 0,
            speaker_directions: vec! (),
            ambisonics_order: 0,
            ambisonics_ordering: AmbisonicsOrdering::FurseMalham,
            ambisonics_normalization: AmbisonicsNormalization::FurseMalham,
            channel_order: ChannelOrder::Interleaved
        }
    }
}

/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLerror;

// Converts an instance of IPLerror into an error string.
pub (crate) fn get_ipl_error_string (ipl_error: IPLerror) -> String {

    format! ("IPL ERROR: {}", match ipl_error {

        IPLerror::IPL_STATUS_SUCCESS        => "The operation completed successfully",
        IPLerror::IPL_STATUS_FAILURE        => "An unspecified error occurred",
        IPLerror::IPL_STATUS_OUTOFMEMORY    => "The system ran out of memory",
        IPLerror::IPL_STATUS_INITIALIZATION => "An error occurred while initializing an external dependency"
    })
}

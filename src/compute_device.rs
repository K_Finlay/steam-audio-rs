/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::{iplCreateComputeDevice, iplDestroyComputeDevice, IPLhandle, IPLerror};
use crate::{Context, ComputeDeviceFilter};
use crate::util::get_ipl_error_string;
use std::ptr;

pub struct ComputeDevice {
    pub (crate) handle: IPLhandle
}

impl ComputeDevice {

    pub fn new (context: &Context, filter: ComputeDeviceFilter) -> Result<ComputeDevice, String> {

        // Create a new compute device handle.
        let mut handle = ptr::null_mut ();
        let result = unsafe {
            iplCreateComputeDevice (context.handle, filter.as_ipl (), &mut handle)
        };

        // If the handle was successfully created, create a new context instance and return.
        // Otherwise, return and error.
        if result == IPLerror::IPL_STATUS_SUCCESS {
            Ok (ComputeDevice { handle })
        }

        else {
            Err (get_ipl_error_string (result))
        }
    }
}

impl Drop for ComputeDevice {

    fn drop (&mut self) {

        unsafe {
            iplDestroyComputeDevice (&mut self.handle);
        }

        self.handle = ptr::null_mut ();
    }
}

/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLVector3;

/// A point or vector in 3D space.
///
/// Phonon uses a right-handed coordinate system, with the positive x-axis pointing right, the positive y-axis pointing up, and the negative z-axis pointing ahead.</br>
/// Position and direction data obtained from a game engine or audio engine must be properly transformed before being passed to any Phonon API function.
pub type Vector3 = IPLVector3;

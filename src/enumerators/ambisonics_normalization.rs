/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLAmbisonicsNormalization;

/// Normalization conventions for Ambisonics channels.
///
/// There are a few different ways of normalizing the values of the Ambisonics channels relative to each other.</br>
/// Phonon supports the most popular ones.
#[derive (Clone, Debug)]
pub enum AmbisonicsNormalization {

    /// This is the normalization scheme used in Furse-Malham higher-order Ambisonics.
    ///
    /// Each channel is normalized to not exceed 1.0, and a -3 dB gain correction is applied to channel 0.
    FurseMalham,

    /// Also called Schmidt semi-normalized form.
    ///
    /// This is the normalization scheme used in the AmbiX format.
    SN3D,

    /// This normalization scheme is based on the mathematical definition of Ambisonics.
    ///
    /// It is closely related to SN3D by a series of scaling factors.</br>
    /// This normalization scheme is used internally throughout Phonon, and using it results in the fastest performance.
    N3D
}

impl AmbisonicsNormalization {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLAmbisonicsNormalization {

        match self {

            AmbisonicsNormalization::FurseMalham => IPLAmbisonicsNormalization::IPL_AMBISONICSNORMALIZATION_FURSEMALHAM,
            AmbisonicsNormalization::SN3D        => IPLAmbisonicsNormalization::IPL_AMBISONICSNORMALIZATION_SN3D,
            AmbisonicsNormalization::N3D         => IPLAmbisonicsNormalization::IPL_AMBISONICSNORMALIZATION_N3D
        }
    }
}

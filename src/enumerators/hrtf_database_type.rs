/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLHrtfDatabaseType;

/// The type of HRTF database to use for binaural rendering.
///
/// You can either use the built-in HRTF database, or supply your own HRTF data at run-time.
#[derive (Clone, Debug)]
pub enum HrtfDatabaseType {

    /// The built-in HRTF database.
    Default,

    /// The built-in HRTF database.
    ///
    /// SOFA is an AES standard file format for storing and exchanging acoustic data, including HRTFs.</br>
    /// For more information on the SOFA format, see https://www.sofaconventions.org/
    Sofa
}

impl HrtfDatabaseType {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLHrtfDatabaseType {

        match self {

            HrtfDatabaseType::Default => IPLHrtfDatabaseType::IPL_HRTFDATABASETYPE_DEFAULT,
            HrtfDatabaseType::Sofa    => IPLHrtfDatabaseType::IPL_HRTFDATABASETYPE_SOFA,
        }
    }
}

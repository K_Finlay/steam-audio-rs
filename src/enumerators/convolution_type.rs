/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLConvolutionType;

#[derive (Clone, Debug)]
pub enum ConvolutionType {

    Phonon,
    TrueAudioNext
}

impl ConvolutionType {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLConvolutionType {

        match self {

            ConvolutionType::Phonon        => IPLConvolutionType::IPL_CONVOLUTIONTYPE_PHONON,
            ConvolutionType::TrueAudioNext => IPLConvolutionType::IPL_CONVOLUTIONTYPE_TRUEAUDIONEXT,
        }
    }
}

/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLHrtfInterpolation;

pub enum HrtfInterpolation {

    Nearest,
    Bilinear
}

impl HrtfInterpolation {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLHrtfInterpolation {

        match self {

            HrtfInterpolation::Nearest => IPLHrtfInterpolation::IPL_HRTFINTERPOLATION_NEAREST,
            HrtfInterpolation::Bilinear    => IPLHrtfInterpolation::IPL_HRTFINTERPOLATION_BILINEAR,
        }
    }
}

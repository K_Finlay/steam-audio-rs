/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLChannelOrder;

/// Whether the data is interleaved or deinterleaved.
#[derive (Clone, Debug)]
pub enum ChannelOrder {

    /// Sample values for each channel are stored one after another, followed by the next set of sample values for each channel, etc.
    ///
    /// In the case of 2-channel stereo, this would correspond to **LRLRLRLR...**
    Interleaved,

    /// All sample values for the first channel are stored one after another, followed by the sample values for the next channel, etc.
    ///
    /// In the case of 2-channel stereo, this would correspond to **LLLL...RRRR...**
    Deinterleaved
}

impl ChannelOrder {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLChannelOrder {

        match self {

            ChannelOrder::Interleaved   => IPLChannelOrder::IPL_CHANNELORDER_INTERLEAVED,
            ChannelOrder::Deinterleaved => IPLChannelOrder::IPL_CHANNELORDER_DEINTERLEAVED
        }
    }
}

/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLChannelLayout;

/// The type of speaker configuration, for audio formats that are not encoded using Ambisonics.
#[derive (Clone, Debug)]
pub enum ChannelLayout {

    /// A single speaker, typically in front of the user.
    Mono,

    /// A pair of speakers, one to the left of the user, and one to the right.
    ///
    /// This is also the setting to use when playing audio over headphones.
    Stereo,

    /// Four speakers: front left, front right, back left, and back right.
    Quadraphonic,

    /// Six speakers: front left, front center, front right, back left, back right, and subwoofer.
    FivePointOne,

    /// Eight speakers: front left, front center, front right, side left, side right, back left,
    /// back right, and subwoofer.
    SevenPointOne,

    /// Lets you specify your own speaker configuration.
    ///
    /// You can specify any number of speakers, and set their positions relative to the user.</br>
    /// This is useful if you have a large speaker array, or if you want Phonon to account for the heights at which the speakers have been installed.
    Custom
}

impl ChannelLayout {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLChannelLayout {

        match self {

            ChannelLayout::Mono          => IPLChannelLayout::IPL_CHANNELLAYOUT_MONO,
            ChannelLayout::Stereo        => IPLChannelLayout::IPL_CHANNELLAYOUT_STEREO,
            ChannelLayout::Quadraphonic  => IPLChannelLayout::IPL_CHANNELLAYOUT_QUADRAPHONIC,
            ChannelLayout::FivePointOne  => IPLChannelLayout::IPL_CHANNELLAYOUT_FIVEPOINTONE,
            ChannelLayout::SevenPointOne => IPLChannelLayout::IPL_CHANNELLAYOUT_SEVENPOINTONE,
            ChannelLayout::Custom        => IPLChannelLayout::IPL_CHANNELLAYOUT_CUSTOM
        }
    }
}

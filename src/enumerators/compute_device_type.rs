/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLComputeDeviceType;

/// The type of device to use with OpenCL.
///
/// The appropriate OpenCL drivers must be installed on the user's system.</br>
/// Multiple OpenCL drivers may be installed on the same system; in this case the first available driver that exposes the specified kind of device will be used.
#[derive (Clone, Debug)]
pub enum ComputeDeviceType {

    /// Use a CPU device only.
    CPU,

    /// Use a GPU device only.
    GPU,

    /// Use either a CPU or GPU device, whichever is listed first by the driver.
    Any
}

impl ComputeDeviceType {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLComputeDeviceType {

        match self {

            ComputeDeviceType::CPU => IPLComputeDeviceType::IPL_COMPUTEDEVICE_CPU,
            ComputeDeviceType::GPU => IPLComputeDeviceType::IPL_COMPUTEDEVICE_GPU,
            ComputeDeviceType::Any => IPLComputeDeviceType::IPL_COMPUTEDEVICE_ANY
        }
    }
}

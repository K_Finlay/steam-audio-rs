/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLChannelLayoutType;

/// Whether the audio buffer is encoded using Ambisonics or not.
#[derive (Clone, Debug)]
pub enum ChannelLayoutType {

    /// Indicates that each channel of audio data is intended to be played back by a single speaker.
    ///
    /// This corresponds to most multi-speaker mono, stereo, or surround sound configurations.
    Speakers,

    /// Indicates that each channel of audio data is to be interpreted as a series of Ambisonics coefficients.
    ///
    /// Playing back such an audio buffer requires a software or hardware Ambisonics decoder.</br>
    /// Phonon contains a software Ambisonics decoder.
    Ambisonics
}

impl ChannelLayoutType {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLChannelLayoutType {

        match self {

            ChannelLayoutType::Speakers   => IPLChannelLayoutType::IPL_CHANNELLAYOUTTYPE_SPEAKERS,
            ChannelLayoutType::Ambisonics => IPLChannelLayoutType::IPL_CHANNELLAYOUTTYPE_AMBISONICS
        }
    }
}

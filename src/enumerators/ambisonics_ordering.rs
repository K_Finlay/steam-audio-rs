/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLAmbisonicsOrdering;

/// The order in which Ambisonics channels are stored in an audio buffer.
#[derive (Clone, Debug)]
pub enum AmbisonicsOrdering {

    /// Specifies the Furse-Malham (FuMa) channel ordering.
    ///
    /// This is an extension of traditional B-format encoding to higher-order Ambisonics.
    FurseMalham,

    /// Specifies the Ambisonics Channel Number scheme for channel ordering.
    ///
    /// This is the new standard adopted by the AmbiX Ambisonics format. </br>
    /// The position of each Ambisonics channel is uniquely calculated as ACN=l2+l+m.
    ACN
}

impl AmbisonicsOrdering {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLAmbisonicsOrdering {

        match self {

            AmbisonicsOrdering::FurseMalham => IPLAmbisonicsOrdering::IPL_AMBISONICSORDERING_FURSEMALHAM,
            AmbisonicsOrdering::ACN         => IPLAmbisonicsOrdering::IPL_AMBISONICSORDERING_ACN
        }
    }
}

/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::{IPLhandle, iplCreateBinauralRenderer, iplDestroyBinauralRenderer, IPLerror};
use crate::{Context, RenderingSettings, HrtfParams, AudioFormat, BinauralEffect};
use crate::util::get_ipl_error_string;
use std::ptr;

pub struct BinauralRenderer {
    pub (crate) handle: IPLhandle
}

impl BinauralRenderer {

    pub fn new (context: &Context, rendering_settings: RenderingSettings, hrtf_params: HrtfParams) -> Result<BinauralRenderer, String> {

        // Create a new context handle
        let mut handle = ptr::null_mut ();
        let result = unsafe {
            iplCreateBinauralRenderer (context.handle, rendering_settings.as_ipl (), hrtf_params.as_ipl (), &mut handle)
        };

        // If the handle was successfully created, create a new context instance and return.
        // Otherwise, return and error.
        if result == IPLerror::IPL_STATUS_SUCCESS {
            Ok (BinauralRenderer { handle })
        }

        else {
            Err (get_ipl_error_string (result))
        }
    }

    pub fn create_binaural_effect (&self, input_format: AudioFormat, output_format: AudioFormat) -> Result<BinauralEffect, String> {
        BinauralEffect::new (&self, input_format, output_format)
    }
}

impl Drop for BinauralRenderer {

    fn drop (&mut self) {

        unsafe {
            iplDestroyBinauralRenderer (&mut self.handle);
        }

        self.handle = ptr::null_mut ();
    }
}

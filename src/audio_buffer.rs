/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::AudioFormat;
use crate::binding::IPLAudioBuffer;
use crate::enumerators::ChannelOrder;
use std::ptr;

pub struct AudioBuffer {

    pub format: AudioFormat,
    pub num_samples: i32,
    pub buffer: Vec<f32>
}

impl AudioBuffer {

    pub fn new (format: AudioFormat, num_samples: i32, buffer: Vec<f32>) -> AudioBuffer {
        AudioBuffer { format, num_samples, buffer }
    }

    pub (crate) fn as_ipl (&self) -> IPLAudioBuffer {

        let interleaved_buffer = match self.format.channel_order {

            ChannelOrder::Interleaved   => self.buffer.as_ptr () as *mut _,
            ChannelOrder::Deinterleaved => ptr::null_mut (),
        };

        let deinterleaved_buffer = match self.format.channel_order {

            ChannelOrder::Interleaved   => ptr::null_mut (),
            ChannelOrder::Deinterleaved => self.buffer.as_ptr () as *mut _,
        };

        IPLAudioBuffer {

            format: self.format.as_ipl (),
            numSamples: self.num_samples,
            interleavedBuffer: interleaved_buffer,
            deinterleavedBuffer: deinterleaved_buffer
        }
    }
}

/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::{IPLhandle, iplCreateBinauralEffect, iplApplyBinauralEffect, iplDestroyBinauralEffect, IPLerror};
use crate::{BinauralRenderer, AudioFormat, AudioBuffer, Vector3};
use crate::util::get_ipl_error_string;
use std::ptr;
use crate::enumerators::HrtfInterpolation;

pub struct BinauralEffect {

    pub (crate) handle: IPLhandle,
    pub (crate) renderer_handle: IPLhandle
}

impl BinauralEffect {

    pub fn new (renderer: &BinauralRenderer, input_format: AudioFormat, output_format: AudioFormat) -> Result<BinauralEffect, String> {

        // Create a new context handle
        let mut handle = ptr::null_mut ();
        let result = unsafe {
            iplCreateBinauralEffect (renderer.handle, input_format.as_ipl (), output_format.as_ipl (), &mut handle)
        };

        // If the handle was successfully created, create a new context instance and return.
        // Otherwise, return and error.
        if result == IPLerror::IPL_STATUS_SUCCESS {
            Ok (BinauralEffect { handle, renderer_handle: renderer.handle })
        }

        else {
            Err (get_ipl_error_string (result))
        }
    }

    pub fn apply_binaural_effect (&self, buffer: AudioBuffer, effect_pos: Vector3, interpolation: HrtfInterpolation) -> AudioBuffer {

        // Convert the data structures into the IPL version.
        let buffer_ipl = buffer.as_ipl ();

        // Make the call to apply the audio effect.
        unsafe {
            iplApplyBinauralEffect (self.handle, self.renderer_handle, buffer_ipl, effect_pos, interpolation.as_ipl (), buffer_ipl);
        }

        // Now, we can just return the original buffer.
        // The as_ipl function for the buffer returns a pointer to the internal data which is then manipulated
        // by the iplApplyBinauralEffectCall, which means the data in the original buffer should have changed as well.
        buffer
    }
}

impl Drop for BinauralEffect {

    fn drop (&mut self) {

        unsafe {
            iplDestroyBinauralEffect (&mut self.handle);
        }

        self.handle = ptr::null_mut ();
    }
}

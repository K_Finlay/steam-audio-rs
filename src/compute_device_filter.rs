/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::IPLComputeDeviceFilter;
use crate::enumerators::ComputeDeviceType;

/// Specifies constraints on the type of OpenCL device to create.
///
/// This information is intended to be passed to iplCreateComputeDevice.
pub struct ComputeDeviceFilter {

    /// The type of device to use.
    pub device_type: ComputeDeviceType,

    /// The maximum number of GPU compute units (CUs) that the application will reserve on the device.
    pub max_cu_to_reserve: i32,

    /// Fraction of maximum reserved CUs that should be used for impulse response (IR) update.
    pub fraction_cu_for_ir_update: f32
}

impl ComputeDeviceFilter {

    pub fn new (device_type: ComputeDeviceType, max_cu_to_reserve: i32, fraction_cu_for_ir_update: f32) -> ComputeDeviceFilter {
        ComputeDeviceFilter { device_type, max_cu_to_reserve, fraction_cu_for_ir_update }
    }
}

impl ComputeDeviceFilter {

    // Convert this object to its IPL equivalent.
    pub (crate) fn as_ipl (&self) -> IPLComputeDeviceFilter {

        IPLComputeDeviceFilter {

            type_: self.device_type.as_ipl (),
            maxCUsToReserve: self.max_cu_to_reserve,
            fractionCUsForIRUpdate: self.fraction_cu_for_ir_update
        }
    }
}

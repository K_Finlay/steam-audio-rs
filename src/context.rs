/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use crate::binding::{iplCreateContext, iplDestroyContext, IPLhandle, IPLerror};
use std::ptr;
use crate::util::get_ipl_error_string;
use crate::{RenderingSettings, HrtfParams, BinauralRenderer};

// TODO: Support logCallback.
// TODO: Support allocateCallback.
// TODO: Support freeCallback.
pub struct Context {
    pub (crate) handle: IPLhandle
}

impl Context {

    pub fn new () -> Result<Context, String> {

        // Create a new context handle
        let mut handle = ptr::null_mut ();
        let result = unsafe {
            iplCreateContext (None, None, None, &mut handle)
        };

        // If the handle was successfully created, create a new context instance and return.
        // Otherwise, return and error.
        if result == IPLerror::IPL_STATUS_SUCCESS {
            Ok (Context { handle })
        }

        else {
            Err (get_ipl_error_string (result))
        }
    }

    pub fn create_binaural_renderer (&self, rendering_settings: RenderingSettings, hrtf_params: HrtfParams) -> Result<BinauralRenderer, String> {
        BinauralRenderer::new (&self, rendering_settings, hrtf_params)
    }
}

impl Drop for Context {

    fn drop (&mut self) {

        unsafe {
            iplDestroyContext (&mut self.handle);
        }

        self.handle = ptr::null_mut ();
    }
}

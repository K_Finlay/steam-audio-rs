/*===============================================================================================*/
// Copyright (c) 2019 Kyle Finlay.
//
// This work is licensed under the terms of the MIT license.
// See <https://opensource.org/licenses/MIT> for more information.
/*===============================================================================================*/

use std::env;
use std::process::Command;
use std::path::Path;

const STEAM_AUDIO_VERSION: &'static str = "2.0-beta.17";

fn main () {

    let download_url = format!  ("https://github.com/ValveSoftware/steam-audio/releases/download/v{0}/steamaudio_api_{0}.zip", STEAM_AUDIO_VERSION);

    let manifest_dir    = env::var ("CARGO_MANIFEST_DIR").unwrap ();
    let ffi_dir         = format!  ("{}/ffi", manifest_dir);
    let steam_audio_dir = format!  ("{}/steamaudio_api", ffi_dir);

    let target_os = env::var ("CARGO_CFG_TARGET_OS");
    let target_os = match target_os.as_ref ().map (|x| &**x) {

        Ok ("linux")   => "Linux",
        Ok ("macos")   => "MacOS",
        Ok ("windows") => "Windows",
        other => panic! ("Unknown target OS {:?}.", other)
    };

    let target_arch = env::var ("CARGO_CFG_TARGET_ARCH");
    let target_arch = match target_arch.as_ref ().map (|x| &**x) {

        Ok ("x86")    => "x86",
        Ok ("x86_64") => "x64",
        other => panic! ("Unknown target architecture {:?}", other)
    };

    if !Path::new (&steam_audio_dir).exists () {
        download_ffi_dependencies (download_url.as_str (), ffi_dir.as_str ());
    }

    bind_gen_steam_audio_and_link (steam_audio_dir.as_str (), target_os, target_arch);
}

fn bind_gen_steam_audio_and_link (steam_audio_dir: &str, target_os: &str, target_arch: &str) {

    let steam_audio_lib_dir = match target_os {

        "Windows" => format! ("{}/lib/Windows/{}/", steam_audio_dir, target_arch),
        "MacOS"   => format! ("{}/lib/OSX/",        steam_audio_dir),
        "Linux"   => format! ("{}/lib/Linux/{}/",   steam_audio_dir, target_arch),
        other     => panic!  ("Unknown target architecture {:?}", other)
    };

    let binding_output_path = format! ("{}/binding.rs", env::var ("OUT_DIR").unwrap ());
    let header_path         = format! ("{}/include/phonon.h", steam_audio_dir);

    bindgen::Builder::default ()
        .header (header_path)
        .rustified_enum ("IPL(.*)")
        .generate ()
        .expect ("Failed to generate Steam Audio binding")
        .write_to_file (binding_output_path)
        .expect ("Failed to write Steam Audio binding");

    println! ("cargo:rustc-link-search={}", steam_audio_lib_dir);
    println! ("cargo:rustc-link-lib=phonon");
}

fn download_ffi_dependencies (download_url: &str, ffi_dir: &str) {

    let download_path = ".steam-audio.zip";

    if cfg! (windows) {

        run_command ("powershell", &[

            "-NoProfile", "-NonInteractive",
            "-Command", "& {

                $client = New-Object System.Net.WebClient
                $client.DownloadFile ($args[0], $args[1])
                if (!$?) { Exit 1 }
                Expand-Archive $args[1] -DestinationPath $args[2]
                Remove-Item –path $args
            }", download_url, download_path, ffi_dir
        ]);
    }

    else {

        run_command ("curl",  &["-L", download_url, "-o", download_path]);
        run_command ("unzip", &["-q", "-o", download_path, "-d", ffi_dir]);
        run_command ("rm",    &["-f", download_path]);
    }
}

fn run_command (cmd: &str, args: &[&str]) {

    match Command::new (cmd).args (args).output () {

        Ok (output) => {

            if !output.status.success () {

                let error = std::str::from_utf8 (&output.stderr).unwrap ();
                panic! ("Command '{}' failed: {}", cmd, error);
            }
        }

        Err (error) => {
            panic! ("Error running command '{}': {:#}", cmd, error);
        }
    }
}
